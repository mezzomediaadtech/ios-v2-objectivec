//
//  AppDelegate.h
//  Manplus_Sample
//
//  Created by MezzoMedia on 2019. 6. 5..
//  Copyright © 2019년 MezzoMedia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

