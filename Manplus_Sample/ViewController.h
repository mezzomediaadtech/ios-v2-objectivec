//
//  ViewController.h
//  Manplus_Sample
//
//  Created by MezzoMedia on 2019. 6. 5..
//  Copyright © 2019년 MezzoMedia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ManBannerController.h"
#import "ManInterstitialController.h"
#import "ManVideoController.h"

@interface ViewController : UIViewController {
    NSString *appID;        //Application ID
    NSString *appName;      //Application Name
    NSString *storeURL;     //App store URL
    
    bool autoplay;     //AutoPlay video ad
    bool autoReplay;   //Auto-replay video ad
    bool muted;        //Play video ad with muted
    bool clickFull;    //Set click area to land on ad details page
    bool viewability;  //Stop the video if the ad area appears to be less than 20% when scrolling through the page
    bool closeBtnShow; //Close button clears ad area
    bool soundBtnShow; //Button to control sound
    bool clickBtnShow; //Ad click button
    bool skipBtnShow;  //Ad skip button
    bool clickVideoArea; //Video area disappears when clicked
}

@end

