//
//  ManJSONKit.h
//  Manplus
//
//  Created by MezzoMedia on 2019. 5. 17..
//  Copyright © 2019년 MezzoMedia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSONKit.h"

// !! 라이브러리를 포함하여 컴파일시 JSONKit duplicate symbols 에러가 발생할수 있어서 상속을 받고 JSONKit.h/.m 파일들을 프로젝트에서 제외한다.
//    단! JSONKit.h/.m import한 경로에 있어야 한다.
@interface ManJSONDecoder : JSONDecoder

@end
